<?php namespace Tsawler\MMPackage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;


/**
 * Class LetsTalkController
 * @package Tsawler\MMPackage
 */
class LetsTalkController extends Controller
{

    /**
     * @param Request $request
     * @return mixed
     */
    public function postLetsTalk(Request $request)
    {
        if (env('APP_DEBUG')) {
            $this->validate($request, [
                'first_name' => 'required',
                'last_name'  => 'required',
                'email'      => 'email',
                'phone'      => 'required',
            ]);
        } else {
            $this->validate($request, [
                'first_name'           => 'required',
                'last_name'            => 'required',
                'email'                => 'email',
                'phone'                => 'required',
                'g-recaptcha-response' => 'required|captcha',
            ]);
        }


//        Mail::to('mmallory@tec-canada.com')
//            ->queue(new CreditAppThanksMailable(Input::get('first_name')));

        $data = [
            'first_name'    => Input::get('first_name'),
            'last_name'     => Input::get('last_name'),
            'phone'         => Input::get('phone'),
            'email_address' => Input::get('email'),
            'comments'      => Input::get('comments'),
        ];

        Mail::to('mmallory@tec-canada.com')->queue(new LetsTalkMailable($data));

        // give reponse
        return Redirect::to('/lets-talk-thanks');
    }

}
