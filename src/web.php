<?php

Route::group(['middleware' => ['web']], function () {
    Route::post('/mm/lets-talk', '\Tsawler\MMPackage\LetsTalkController@postLetsTalk');
});
