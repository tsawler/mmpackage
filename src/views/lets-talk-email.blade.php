@component('mail::message')
# Contact from Website

The following contact came in from the website:

Contact name: {{ $first_name }} {{ $last_name }}

Contact email: {{ $email_address }}

Phone: {{ $phone }}

Comments:

{{ $comments }}
@endcomponent