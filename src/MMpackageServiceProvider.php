<?php namespace Tsawler\MMPackage;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

/**
 * Class MMpackageServiceProvider
 * @package Tsawler\MMPackage;
 */
class MMpackageServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *c
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/web.php');
        $this->loadViewsFrom(__DIR__ . '/views', 'mmpackage');
    }


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

}