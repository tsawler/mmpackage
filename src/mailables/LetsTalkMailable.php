<?php namespace Tsawler\MMPackage;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LetsTalkMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $first_name;
    public $last_name;
    public $email_address;
    public $phone;
    public $comments;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->first_name = $data['first_name'];
        $this->last_name = $data['last_name'];
        $this->email_address = $data['email_address'];
        $this->phone = $data['phone'];
        $this->comments = $data['comments'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mmpackage::lets-talk-email')
            ->subject("Contact from website");
    }
}